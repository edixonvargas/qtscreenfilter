#include "coloreditor.h"

ColorEditor::ColorEditor(QColor *rgb, double *a, QWidget *parent) :
	QWidget(parent)
{
	lblRed = new QLabel("Red");
	lblGreen = new QLabel("Green");
	lblBlue = new QLabel("Blue");
	lblAlpha = new QLabel("Opacity");

	sRed = new QSlider(Qt::Horizontal);
	sGreen = new QSlider(Qt::Horizontal);
	sBlue = new QSlider(Qt::Horizontal);
	sAlpha = new QSlider(Qt::Horizontal);

	formMain = new QFormLayout();

	rgbVal = rgb;

	alpha = a;

	sRed->setMaximum(255);
	sRed->setValue(rgb->red());

	sGreen->setMaximum(255);
	sGreen->setValue(rgb->green());

	sBlue->setMaximum(255);
	sBlue->setValue(rgb->blue());

	sAlpha->setMaximum(255);
	sAlpha->setValue((*a) * sAlpha->maximum());

	formMain->addRow(lblRed, sRed);
	formMain->addRow(lblGreen, sGreen);
	formMain->addRow(lblBlue, sBlue);
	formMain->addRow(lblAlpha, sAlpha);

	setLayout(formMain);

	connect(sRed, SIGNAL(valueChanged(int)), SLOT(onRedChanged(int)));
	connect(sGreen, SIGNAL(valueChanged(int)), SLOT(onGreenChanged(int)));
	connect(sBlue, SIGNAL(valueChanged(int)), SLOT(onBlueChanged(int)));
	connect(sAlpha, SIGNAL(valueChanged(int)), SLOT(onAlphaChanged(int)));

}

ColorEditor::~ColorEditor()
{
	delete lblRed;
	delete lblGreen;
	delete lblBlue;
	delete lblAlpha;
	delete sRed;
	delete sGreen;
	delete sBlue;
	delete sAlpha;
	delete formMain;
}

void ColorEditor::closeEvent(QCloseEvent *e)
{
	hide();
	e->ignore();
}

void ColorEditor::onRedChanged(int val)
{
	rgbVal->setRed(val);
	sRed->setToolTip(QString::number(val));
}

void ColorEditor::onGreenChanged(int val)
{
	rgbVal->setGreen(val);
	sGreen->setToolTip(QString::number(val));
}

void ColorEditor::onBlueChanged(int val)
{
	rgbVal->setBlue(val);
	sBlue->setToolTip(QString::number(val));
}

void ColorEditor::onAlphaChanged(int val)
{
	*alpha = double(val)/double(sAlpha->maximum());
	sAlpha->setToolTip(QString::number(*alpha));
}
