#-------------------------------------------------
#
# Project created by QtCreator 2016-05-18T04:14:47
#
#-------------------------------------------------

QT       += core gui widgets

CONFIG += widgets core

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Filter
TEMPLATE = app


SOURCES += main.cpp\
        screenfilter.cpp \
    coloreditor.cpp

HEADERS  += screenfilter.h \
    coloreditor.h

RESOURCES += \
    media.qrc
