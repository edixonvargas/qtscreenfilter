#ifndef SCREENFILTER_H
#define SCREENFILTER_H

#include <QtCore>
#include <QtWidgets>
#include <QtGui>

#include "coloreditor.h"
//class QFocusEvent;

class ScreenFilter : public QWidget
{
	public:
		explicit ScreenFilter(QWidget *parent = 0);

		~ScreenFilter();

	private slots:
		void onTimeout();
		void onActivated(QSystemTrayIcon::ActivationReason reason); 
		void onEditFilterTrigger();
		void onExitTriggered();

	private:
		Q_OBJECT

		QDesktopWidget *dw;

		QMenu *contextMenu;

		double opacity;

		QColor rgbVal;

		QSystemTrayIcon *sti;
		QTimer *timer;

		ColorEditor *colorEditor;
};

#endif // SCREENFILTER_H
