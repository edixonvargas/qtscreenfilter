#ifndef COLOREDITOR_H
#define COLOREDITOR_H

#include <QtWidgets>

class ColorEditor : public QWidget
{
	public:
		explicit ColorEditor(QColor *rgb, double *a, QWidget *parent = 0);

		~ColorEditor();
		
	protected:
		void closeEvent(QCloseEvent *e);

	signals:

	private slots:
		void onRedChanged(int val);
		void onGreenChanged(int val);
		void onBlueChanged(int val);
		void onAlphaChanged(int val);

	private:
		Q_OBJECT
		
		double *alpha;

		QColor *rgbVal;

		QLabel
		*lblRed,
		*lblGreen,
		*lblBlue,
		*lblAlpha;

		QSlider
		*sRed,
		*sGreen,
		*sBlue,
		*sAlpha;

		QFormLayout *formMain;
};

#endif // COLOREDITOR_H
