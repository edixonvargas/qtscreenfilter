#include "screenfilter.h"

ScreenFilter::ScreenFilter(QWidget *parent)
	: QWidget(parent, Qt::Tool)
{
	dw = QApplication::desktop();
	timer = new QTimer();

	QPalette palette = this->palette();

	rgbVal = qRgb(255,110,0);
	opacity = 0.15;


	palette.setColor(QPalette::Background, rgbVal);
	setPalette(palette);
	setBackgroundRole(QPalette::Background);
	setWindowOpacity(opacity);

	setWindowState(Qt::WindowFullScreen | Qt::WindowActive);
	setWindowModality(Qt::WindowModal);
	setAttribute(Qt::WA_DeleteOnClose, true);

	setWindowFlags(
//				Qt::CustomizeWindowHint |
//				Qt::WindowSystemMenuHint |
//				Qt::Popup |
				Qt::ToolTip |
//				Qt::Tool |
				Qt::WindowTransparentForInput |
//				Qt::WindowDoesNotAcceptFocus |
				Qt::FramelessWindowHint |
				Qt::WindowStaysOnTopHint |
				Qt::BypassWindowManagerHint
//				Qt::X11BypassWindowManagerHint
				);
//	setFixedSize(dw->screen()->size());

//	timer->setInterval(500);
	timer->start(5000);
	
	colorEditor = new ColorEditor(&rgbVal, &opacity);

	sti = new QSystemTrayIcon(QIcon("://media/Blue-Light-Screen-Filter-120x120.png"));

	contextMenu = new QMenu();
	contextMenu->addAction("Edit filter", this, SLOT(onEditFilterTrigger()));
	contextMenu->addAction("Exit", this, SLOT(onExitTriggered()));

	sti->setContextMenu(contextMenu);

	connect(sti, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), SLOT(onActivated(QSystemTrayIcon::ActivationReason)));
	connect(timer, SIGNAL(timeout()), SLOT(onTimeout()));

	sti->show();
//	showFullScreen();
}

ScreenFilter::~ScreenFilter()
{
	delete timer;
	delete colorEditor;
	delete contextMenu;
	delete sti;

}

void ScreenFilter::onTimeout()
{
	QPalette palette = this->palette();
	palette.setColor(QPalette::Background, rgbVal);
	setPalette(palette);
	setBackgroundRole(QPalette::Background);

	setWindowOpacity(opacity);
//	dw->updateGeometry();
//	dw->showMaximized();
//	dw->showFullScreen();

//	sizeScreen->setText("Current size: " + QString::number(dw->screenGeometry().width()) + ", " + QString::number(dw->screenGeometry().height()));
	if(dw->isVirtualDesktop()){
//		dw->screen();
//		windowHandle()->setScreen(qApp->screens().last());
//		setGeometry(dw->screen(dw->primaryScreen())->geometry());
//		setGeometry(dw->screenGeometry());
		setGeometry(dw->geometry());
	}

	raise();
}

void ScreenFilter::onActivated(QSystemTrayIcon::ActivationReason reason)
{
	switch(reason){
		case QSystemTrayIcon::DoubleClick:
			colorEditor->show();
			break;

		case QSystemTrayIcon::Context:
		case QSystemTrayIcon::Unknown:
		case QSystemTrayIcon::Trigger:
		case QSystemTrayIcon::MiddleClick:
			break;
			
	}
}

void ScreenFilter::onEditFilterTrigger()
{

	colorEditor->show();
}

void ScreenFilter::onExitTriggered()
{
	sti->hide();
	close();

	qApp->quit();
//	deleteLater();
}

//void ScreenFilter::?focusOutEvent(QFocusEvent *event)
//{
//	event->ignore();
//}
